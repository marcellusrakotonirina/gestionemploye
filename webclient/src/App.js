import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const GET_EMPLOYEES = gql`
  {
    employees {
      id
      nom
      prenom
    }
  }
`;

class App extends Component {

  render() {
    return (
      <Query pollInterval={500} query={GET_EMPLOYEES}>
        {({ loading, error, data }) => {
          if (loading) return 'Loading...';
          if (error) return `Error! ${error.message}`;
    
          return (
            <div className="container">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-nom">
                    Listes employees
                  </h3>
                  <h4><Link to="/create">Ajout Employee</Link></h4>
                </div>
                <div className="panel-body">
                  <table className="table table-stripe">
                    <thead>
                      <tr>
                        <th>nom</th>
                        <th>prenom</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.employes.map((employee, index) => (
                        <tr key={index}>
                          <td><Link to={`/show/${employee._id}`}>{employee.nom}</Link></td>
                          <td>{employee.nom}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default App;