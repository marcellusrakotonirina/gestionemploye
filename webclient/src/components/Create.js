import React, { Component } from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Link } from 'react-router-dom';

const ADD_EMPLOYEE = gql`
    mutation createEmployee(
        $nom: String!,
        $prenom: String!,
        $age: Int!,
        $poste: String!,
        $experience: String!{
            createEmployee(
                nom: $nom,
                prenom: $prenom,
                age: $age,
                poste: $poste,
                experience: $experience) {
            id
        }
    }
`;

class Create extends Component {
  
    render() {
      let nom, prenom, age, poste, experience;
      return (
        <Mutation mutation={ADD_EMPLOYEE} onCompleted={() => this.props.history.push('/')}>
            {(createEmployee, { loading, error }) => (
                <div className="container">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-prenom">
                                Formulaire d'ajout employee
                            </h3>
                        </div>
                        <div className="panel-body">
                            <h4><Link to="/" className="btn btn-primary">Listes employee</Link></h4>
                            <form onSubmit={e => {
                                e.preventDefault();
                                addBook({ variables: { nom: nom.value, prenom: prenom.value,  poste: poste.value, experience: experience.value, age: parseInt(age.value) } });
                                nom.value = "";
                                prenom.value = "";
                                age.value = "";
                                poste.value = "";
                                experience.value = null;
                                published_year.value = "";
                            }}>
                                <div className="form-group">
                                    <label htmlFor="nom">nom:</label>
                                    <input type="text" className="form-control" name="nom" ref={node => {
                                        nom = node;
                                    }} placeholder="nom" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="prenom">prenom:</label>
                                    <input type="text" className="form-control" name="prenom" ref={node => {
                                        prenom = node;
                                    }} placeholder="prenom" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="age">age:</label>
                                    <input type="text" className="form-control" name="age" ref={node => {
                                        age = node;
                                    }} placeholder="age" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="poste">poste:</label>
                                    <input className="form-control" name="poste" ref={node => {
                                        poste = node;
                                    }} placeholder="poste" cols="80" rows="3" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="age">experience:</label>
                                    <input type="text" className="form-control" name="experience" ref={node => {
                                        experience = node;
                                    }} placeholder="experience" />
                                </div>
                            
                                <button type="submit" className="btn btn-success">Submit</button>
                            </form>
                            {loading && <p>Loading...</p>}
                            {error && <p>Error :( Please try again</p>}
                        </div>
                    </div>
                </div>
            )}
        </Mutation>
      );
    }
  }
  
  export default Create;