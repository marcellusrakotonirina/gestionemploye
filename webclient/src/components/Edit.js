import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";

const GET_EMPLOYEE = gql`
    query employee($Id: String) {
        employee(id: $Id) {
            id
            nom
            prenom
            poste
            experience
            age
        }
    }
`;

const UPDATE_EMPLOYEE = gql`
mutation createEmployee(
    $nom: String!,
    $prenom: String!,
    $age: Int!,
    $poste: String!,
    $experience: String!{
        createEmployee(
            nom: $nom,
            prenom: $prenom,
            age: $age,
            poste: $poste,
            experience: $experience) {
        id
    }
}
`;

class Edit extends Component {

  render() {
    let nom, prenom, poste, experience, age;
    return (
        <Query query={GET_EMPLOYEE} variables={{ Id: this.props.match.params.id }}>
            {({ loading, error, data }) => {
                if (loading) return 'Loading...';
                if (error) return `Error! ${error.message}`;
        
                return (
                    <Mutation mutation={UPDATE_EMPLOYEE} key={data.employee._id} onCompleted={() => this.props.history.push(`/`)}>
                        {(modifyEmployee, { loading, error }) => (
                            <div className="container">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <h3 className="panel-prenom">
                                            Edit Employee
                                        </h3>
                                    </div>
                                    <div className="panel-body">
                                        <h4><Link to="/" className="btn btn-primary">Liste employee</Link></h4>
                                        <form onSubmit={e => {
                                            e.preventDefault();
                                            updateemployee({ variables: { id: data.employee._id, nom: nom.value, prenom: prenom.value, poste: poste.value, experience: experience.value, age: parseInt(age.value) } });
                                            nom.value = "";
                                            prenom.value = "";
                                            poste.value = "";
                                            experience.value = "";
                                            age.value = "";
                                        }}>
                                            <div className="form-group">
                                                <label htmlFor="nom">nom:</label>
                                                <input type="text" className="form-control" name="nom" ref={node => {
                                                    nom = node;
                                                }} placeholder="nom" defaultValue={data.employee.nom} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="prenom">prenom:</label>
                                                <input type="text" className="form-control" name="prenom" ref={node => {
                                                    prenom = node;
                                                }} placeholder="prenom" defaultValue={data.employee.prenom} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="poste">poste:</label>
                                                <input type="text" className="form-control" name="poste" ref={node => {
                                                    poste = node;
                                                }} placeholder="poste" defaultValue={data.employee.poste} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="experience">experience:</label>
                                                <textarea className="form-control" name="experience" ref={node => {
                                                    experience = node;
                                                }} placeholder="experience" cols="80" rows="3" defaultValue={data.employee.experience} />
                                            </div>
                                           
                                            <div className="form-group">
                                                <label htmlFor="poste">Age:</label>
                                                <input type="number" className="form-control" name="age" ref={node => {
                                                    age = node;
                                                }} placeholder="Published Year" defaultValue={data.employee.age} />
                                            </div>
                                            <button type="submit" className="btn btn-success">Submit</button>
                                        </form>
                                        {loading && <p>Loading...</p>}
                                        {error && <p>Error :( SVP, essayez encore</p>}
                                    </div>
                                </div>
                            </div>
                        )}
                    </Mutation>
                );
            }}
        </Query>
    );
  }
}

export default Edit;