import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';

const GET_EMPLOYEE = gql`
    query employee($Id: String) {
        employee(id: $Id) {
            id
            nom
            prenom
            poste
            experience
            age
          
        }
    }
`;

const DELETE_EMPLOYEE = gql`
  mutation deleteEmployee($id: String!) {
    deleteEmployee(id:$Id) {
      id
    }
  }
`;

class Show extends Component {

  render() {
    return (
        <Query pollInterval={500} query={GET_EMPLOYEE} variables={{ Id: this.props.match.params.id }}>
            {({ loading, error, data }) => {
                if (loading) return 'Loading...';
                if (error) return `Error! ${error.message}`;
        
                return (
                    <div className="container">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                            <h4><Link to="/">Liste employee</Link></h4>
                                <h3 className="panel-prenom">
                                {data.employee.prenom}
                                </h3>
                            </div>
                            <div className="panel-body">
                                <dl>
                                    <dt>nom:</dt>
                                    <dd>{data.employee.nom}</dd>
                                    <dt>poste:</dt>
                                    <dd>{data.employee.poste}</dd>
                                    <dt>experience:</dt>
                                    <dd>{data.employee.experience}</dd>
                                    <dt>age:</dt>
                                    <dd>{data.employee.age}</dd>
                                </dl>
                                <Mutation mutation={DELETE_employee} key={data.employee.id} onCompleted={() => this.props.history.push('/')}>
                                    {(removeemployee, { loading, error }) => (
                                        <div>
                                            <form
                                                onSubmit={e => {
                                                    e.preventDefault();
                                                    removeemployee({ variables: { id: data.employee.id } });
                                                }}>
                                                <Link to={`/edit/${data.employee.id}`} className="btn btn-success">Edit</Link>&nbsp;
                                                <button type="submit" className="btn btn-danger">Delete</button>
                                            </form>
                                        {loading && <p>Loading...</p>}
                                        {error && <p>Error :( Please try again</p>}
                                        </div>
                                    )}
                                </Mutation>
                            </div>
                        </div>
                    </div>
                );
            }}
        </Query>
    );
  }
}

export default Show;