const { GraphQLServer } = require('./node_modules/graphql-yoga')
const morgan = require('./node_modules/morgan')
const cors = require('./node_modules/cors')
const resolvers = require('./resolvers')

const { connect } = require('./models/db')
require('./config/config');

const typeDefs = 'src/schema.graphql'
const server = new GraphQLServer({
    typeDefs,
    resolvers,
    context: async req => ({
        ...req,
        mongo: await connect() 
    })
})

server.express.use(cors())
server.express.use(morgan('dev'))

// error handler
app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    }
    else{
        console.log(err);
    }
});

// start server
app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));