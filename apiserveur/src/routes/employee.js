const express = require('express');

const router = express.Router();
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

const employeequery = require('../requette/Query');
const employeeCtrl = require('../resolvers/employe');

router.get('/',  employeequery);
router.post('/', employeeCtrl);
router.get('/:id', employeeCtrl.getOneEmployee);
router.put('/:id', employeeCtrl.modifyEmployee);
router.delete('/:id',  employeeCtrl.deleteEmployee);

module.exports = router;