const Query = {

  hello(root, args, context) {
    return 'Hello World!'
  },

  async getAllEmployees (parent, args, ctx) {
    const collection = ctx.mongo.db.collection('employes')
    const employes = await collection.find({}).toArray()
    return employes
  },

  async getOneEmployee (parent, { id }, ctx) {
    const collection = ctx.mongo.db.collection('employes')
    return collection.findOne({ id })
  },

  async getAllExperiences (parent, args, ctx) {
    const collection = ctx.mongo.db.collection('experiences')
    const experiences = await collection.find({}).toArray()
    return experiences
  },

  async getOneExperience (parent, { id }, ctx) {
    const collection = ctx.mongo.db.collection('experiences')
    return collection.findOne({ id })
  },

}

module.exports = { Query }